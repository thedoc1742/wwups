[self]
uid=default
gid=f5185570-dc35-48b6-bf33-bbdcae5fc2ef
uidType=TEMPLATESTORE
class=de.espirit.firstspirit.store.access.templatestore.PageTemplateImpl

[requiredId]
uid=accordion
gid=aef87fa0-6f8e-4203-8b0f-67f87b79a92b
uidType=TEMPLATESTORE
class=de.espirit.firstspirit.store.access.templatestore.SectionTemplateImpl

uid=body_title
gid=a9c8b5f6-9a2a-48cd-9e9c-e17d84d9e17a
uidType=TEMPLATESTORE
class=de.espirit.firstspirit.store.access.templatestore.SectionTemplateImpl

uid=galerie
gid=a468b5f9-0d8c-45c6-af30-69b3e809e3fe
uidType=TEMPLATESTORE
class=de.espirit.firstspirit.store.access.templatestore.SectionTemplateImpl

uid=mithras.gallery_overview
gid=c828cf84-4dfb-4577-a3be-a23c8d1f10fe
uidType=TEMPLATESTORE_SCHEMA
class=de.espirit.firstspirit.store.access.templatestore.TableTemplateImpl

uid=mithras.galleries
gid=b8cbcf20-5380-4b59-9b48-8183fc1df3c2
uidType=TEMPLATESTORE_SCHEMA
class=de.espirit.firstspirit.store.access.templatestore.TableTemplateImpl

uid=mithras.product_categories
gid=cba99ace-f5cf-414a-ad26-16f703f32555
uidType=TEMPLATESTORE_SCHEMA
class=de.espirit.firstspirit.store.access.templatestore.TableTemplateImpl

uid=mithras.news
gid=210c9f70-7218-4607-98f5-0c24802e3f15
uidType=TEMPLATESTORE_SCHEMA
class=de.espirit.firstspirit.store.access.templatestore.TableTemplateImpl

uid=news_teaser
gid=d5edfce7-7ca2-4b4a-90cf-df73105b972a
uidType=TEMPLATESTORE
class=de.espirit.firstspirit.store.access.templatestore.SectionTemplateImpl

uid=mithras.news_overview
gid=c14eb295-992c-4e43-b2a2-a6c683013f99
uidType=TEMPLATESTORE_SCHEMA
class=de.espirit.firstspirit.store.access.templatestore.TableTemplateImpl

uid=mithras.products
gid=fb0c3972-3151-4f28-a13a-70c2f02cda5e
uidType=TEMPLATESTORE_SCHEMA
class=de.espirit.firstspirit.store.access.templatestore.TableTemplateImpl

uid=mithras.products_overview
gid=ba0f47c9-a62f-4380-95b8-20de715bef2b
uidType=TEMPLATESTORE_SCHEMA
class=de.espirit.firstspirit.store.access.templatestore.TableTemplateImpl

uid=location
gid=e707a50f-90c0-4856-9823-12b416e0a5d3
uidType=TEMPLATESTORE
class=de.espirit.firstspirit.store.access.templatestore.SectionTemplateImpl

uid=locationsmap
gid=0ecd0cd0-624d-408c-a8e9-333d1c09370b
uidType=TEMPLATESTORE
class=de.espirit.firstspirit.store.access.templatestore.SectionTemplateImpl

uid=searchresults
gid=e4351452-e125-45b8-a429-0fc900075fe4
uidType=TEMPLATESTORE
class=de.espirit.firstspirit.store.access.templatestore.SectionTemplateImpl

uid=table
gid=ce6b1e74-1aaa-477d-bef5-4606440ca9e0
uidType=TEMPLATESTORE
class=de.espirit.firstspirit.store.access.templatestore.SectionTemplateImpl

uid=teaser
gid=533ea3c9-0a08-4645-a92f-05cec8fda084
uidType=TEMPLATESTORE
class=de.espirit.firstspirit.store.access.templatestore.SectionTemplateImpl

uid=title_text_picture
gid=37593e8e-7142-4af4-83d6-d0114a47ed4f
uidType=TEMPLATESTORE
class=de.espirit.firstspirit.store.access.templatestore.SectionTemplateImpl

uid=product_offers
gid=54233973-500c-4ea3-af70-835d0473799a
uidType=TEMPLATESTORE
class=de.espirit.firstspirit.store.access.templatestore.SectionTemplateImpl

[optionalId]
uid=images
gid=fa4a9a4e-0655-4966-9dcb-a689ec61fb08
uidType=MEDIASTORE_FOLDER
class=de.espirit.firstspirit.store.access.mediastore.MediaFolderImpl

uid=mithrasenergy
gid=0e787e54-f844-4255-a8c3-a741d404c775
uidType=MEDIASTORE_LEAF
class=de.espirit.firstspirit.store.access.mediastore.MediaImpl

uid=additionalequipment_overview
gid=c3a6e2ac-53e1-4063-b96e-76fa936e48dd
uidType=SITESTORE_LEAF
class=de.espirit.firstspirit.store.access.sitestore.PageRefImpl

uid=breadcrumb
gid=e1902537-881a-4cae-9166-0b7911d291bf
uidType=TEMPLATESTORE_FORMATTEMPLATE
class=de.espirit.firstspirit.store.access.templatestore.FormatTemplateImpl

uid=header
gid=7529e993-2d4d-4f9a-86f0-cc1021853741
uidType=TEMPLATESTORE_FORMATTEMPLATE
class=de.espirit.firstspirit.store.access.templatestore.FormatTemplateImpl

uid=headline_h1
gid=011666f9-4872-4c47-98e3-6a574d7fb2bf
uidType=TEMPLATESTORE_FORMATTEMPLATE
class=de.espirit.firstspirit.store.access.templatestore.FormatTemplateImpl

uid=p
gid=57935a33-23dc-445b-ae30-2c52c1fa460b
uidType=TEMPLATESTORE_FORMATTEMPLATE
class=de.espirit.firstspirit.store.access.templatestore.FormatTemplateImpl

uid=page_foot
gid=9fcd11d2-c981-4714-9c1f-b059d1300ac9
uidType=TEMPLATESTORE_FORMATTEMPLATE
class=de.espirit.firstspirit.store.access.templatestore.FormatTemplateImpl

uid=page_head
gid=9e3dda68-85d9-4d5e-9378-78ee310b1bb5
uidType=TEMPLATESTORE_FORMATTEMPLATE
class=de.espirit.firstspirit.store.access.templatestore.FormatTemplateImpl

uid=picture
gid=f2be01ab-5350-4d98-8615-fc6423767e71
uidType=TEMPLATESTORE_FORMATTEMPLATE
class=de.espirit.firstspirit.store.access.templatestore.FormatTemplateImpl

uid=responsive_body
gid=4b4a9edd-f279-494b-a616-f27a02815174
uidType=TEMPLATESTORE_FORMATTEMPLATE
class=de.espirit.firstspirit.store.access.templatestore.FormatTemplateImpl

uid=responsive_iconpanel
gid=43a0311d-8efd-49c5-9c5f-0f64735db427
uidType=TEMPLATESTORE_FORMATTEMPLATE
class=de.espirit.firstspirit.store.access.templatestore.FormatTemplateImpl

uid=footertext
gid=3759df82-fada-43ae-95c9-9857a414f790
uidType=GLOBALSTORE
class=de.espirit.firstspirit.store.access.globalstore.GCAPageImpl


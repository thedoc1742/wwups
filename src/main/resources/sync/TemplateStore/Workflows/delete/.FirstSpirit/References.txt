[self]
uid=delete
gid=175c7584-e5ae-4887-b9b9-7a334bcbe358
uidType=TEMPLATESTORE
class=de.espirit.firstspirit.store.access.templatestore.WorkflowImpl

[optionalId]
uid=wf_delete
gid=a790b1bd-1c32-4dbf-aef9-c6178df16db6
uidType=TEMPLATESTORE
class=de.espirit.firstspirit.store.access.templatestore.ScriptImpl

uid=wf_delete_branch_if_folder
gid=85649055-22ee-4733-81e7-e35668abc2eb
uidType=TEMPLATESTORE
class=de.espirit.firstspirit.store.access.templatestore.ScriptImpl

uid=wf_delete_check_prerequisites
gid=03232dcd-54b5-48bd-a5e2-7d834af73e21
uidType=TEMPLATESTORE
class=de.espirit.firstspirit.store.access.templatestore.ScriptImpl

uid=wf_delete_find_releated_objects
gid=1644c9b5-36ea-4da5-9415-c3f094f88563
uidType=TEMPLATESTORE
class=de.espirit.firstspirit.store.access.templatestore.ScriptImpl

uid=wf_delete_show_related
gid=1697d9c0-da30-4fc1-912e-00005e8d266e
uidType=TEMPLATESTORE
class=de.espirit.firstspirit.store.access.templatestore.ScriptImpl

uid=wf_delete_show_warning
gid=f6bbe8d1-cd0b-4d29-9d13-d06e3103c283
uidType=TEMPLATESTORE
class=de.espirit.firstspirit.store.access.templatestore.ScriptImpl

uid=wf_delete_showactiondialog
gid=87074f93-5d4a-4ad2-b6a4-15d8a8096995
uidType=TEMPLATESTORE
class=de.espirit.firstspirit.store.access.templatestore.ScriptImpl

uid=wf_delete_test
gid=8c621842-35e1-4804-9f1d-5b2517eedf8b
uidType=TEMPLATESTORE
class=de.espirit.firstspirit.store.access.templatestore.ScriptImpl

uid=wf_start_deltadeployment
gid=f1956588-51da-4964-9896-2d0ac46f5aa1
uidType=TEMPLATESTORE
class=de.espirit.firstspirit.store.access.templatestore.ScriptImpl

